#!/bin/bash

set -eu

echo "=> Ensure directories"
mkdir -p /app/data/bedrock/worlds /app/data/bedrock/behavior_packs /app/data/bedrock/resource_packs

# CLOUDRON_OIDC_PROVIDER_NAME is not supported

if [[ ! -f /app/data/bedrock/server.properties ]]; then
    cp /app/code/server.properties.template /app/data/bedrock/server.properties
fi

# https://bugs.mojang.com/browse/BDS-12751
if ! grep -q ^server-portv6 /app/data/bedrock/server.properties; then
    echo -e "\nserver-portv6=19133\n" >> /app/data/bedrock/server.properties
fi

if [[ ! -f /app/data/bedrock/permissions.json ]]; then
    echo "[]" > /app/data/bedrock/permissions.json
fi

if [[ ! -f /app/data/bedrock/allowlist.json ]]; then
    echo "[]" > /app/data/bedrock/allowlist.json
fi

echo "=> Set ports to ${BEDROCK_PORT_V4_TCP}"
sed -e "s/server-port=.*/server-port=${BEDROCK_PORT_V4_TCP}/" -i /app/data/bedrock/server.properties

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Starting management server"
export MC_FLAVOR=bedrock
exec /usr/local/bin/gosu cloudron:cloudron node /app/pkg/index.js
