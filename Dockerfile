FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# versions from https://www.minecraft.net/en-us/download/server/bedrock
# renovate: datasource=custom.bedrock depName=bedrock versioning=regex:^(?<major>\d+)\.(?<minor>\d+)\.(?<patch>\d+)\.(?<build>\d+)$ extractVersion=/bin-linux/bedrock-server-(?<version>.+)\.zip$
ARG BEDROCK_VERSION=1.21.62.01

# this server wants a proper user-agent
RUN wget --user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "https://httpbin.io/user-agent" \
    https://www.minecraft.net/bedrockdedicatedserver/bin-linux/bedrock-server-${BEDROCK_VERSION}.zip  && \
    unzip bedrock-server-${BEDROCK_VERSION}.zip && rm bedrock-server-${BEDROCK_VERSION}.zip && chmod +x /app/code/bedrock_server

RUN mv server.properties server.properties.template && ln -s /app/data/bedrock/server.properties /app/code/server.properties && \
    rm permissions.json && ln -s /app/data/bedrock/permissions.json /app/code/permissions.json && \
    rm allowlist.json && ln -s /app/data/bedrock/allowlist.json /app/code/allowlist.json && \
    ln -s /app/data/bedrock/worlds /app/code/worlds && \
    ln -s /app/data/bedrock/behavior_packs /app/code/behavior_packs && \
    ln -s /app/data/bedrock/resource_packs /app/code/resource_packs && \
    ln -s /dev/null /app/code/Debug_Log.txt

COPY shared/frontend /app/pkg/frontend
COPY shared/backend /app/pkg/backend
COPY shared/index.js shared/package.json shared/package-lock.json start.sh /app/pkg/

WORKDIR /app/pkg
RUN npm install

CMD [ "/app/pkg/start.sh" ]
