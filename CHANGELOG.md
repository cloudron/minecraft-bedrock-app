[1.0.0]
* Initial release

[1.0.1]
* Do not overwrite server configs on restart

[1.1.0]
* Update to Bedrock 1.12.0.28

[1.2.0]
* Update to Bedrock 1.12.1.1

[1.3.0]
* Update to Bedrock 1.13.0.34

[2.0.0]
* Update to Bedrock 1.14.60.5

[2.1.0]
* Update to Bedrock 1.16.1.02
* Add forum link to manifest

[2.1.1]
* Update to Bedrock 1.16.40.02

[2.1.2]
* Update to Bedrock 1.16.101.01

[2.1.3]
* Update to Bedrock 1.16.201.02

[2.1.4]
* Update base image to v3

[2.1.5]
* Update to Bedrock 1.16.221.01

[2.2.0]
* Update to Bedrock 1.17.0.03

[2.3.0]
* Update to Bedrock 1.17.10.04

[2.4.0]
* Update to Bedrock 1.17.30.04

[2.4.1]
* Update to Bedrock 1.17.32.02

[2.5.0]
* Update to Bedrock 1.17.40.06

[2.6.0]
* Use manifest v2

[2.7.0]
* Update to Bedrock 1.18.1.02

[2.7.1]
* Fix LDAP to work with manifest v2

[2.7.2]
* Update to Bedrock 1.18.2.03

[2.7.3]
* Update base image to 3.2.0

[2.7.4]
* Update to Bedrock 1.18.11.01
* whitelist.json rename to allowlist.json

[2.7.5]
* Update Bedrock to 1.18.31.04

[2.7.6]
* Update Bedrock to 1.18.32.02
* Fix LDAP connection leak

[2.8.0]
* Update Bedrock to 1.19.1.01

[2.9.0]
* Update Bedrock to 1.19.10.03

[2.10.0]
* Update Bedrock to 1.19.20.02

[2.11.0]
* Update Bedrock to 1.19.21.01

[2.12.0]
* Update Bedrock to 1.19.30.04

[2.12.1]
* Update Bedrock to 1.19.40.02

[2.13.0]
* Update Bedrock to 1.19.50.02

[2.14.0]
* Update Bedrock to 1.19.51.01

[2.15.0]
* Update Bedrock to 1.19.61.01

[2.16.0]
* Update Bedrock to 1.19.63.01

[2.16.1]
* Update Bedrock to 1.19.70.02

[2.17.0]
* Update Bedrock to 1.19.73.02

[2.18.0]
* Update Bedrock to 1.19.81.01

[2.19.0]
* Update Bedrock to 1.20.10.21

[2.20.0]
* Update Bedrock to 1.20.0.01

[2.21.0]
* Update Bedrock to 1.20.11.01

[2.21.1]
* Fix duplicate log lines in console
* Clear username/password on logout

[2.22.0]
* OIDC support
* Fix memory limit computation (cgroup v2)
* Duplicate log lines
* Better log lines in the ui
* Log history

[2.22.1]
* Update Bedrock to 1.20.14.01

[2.22.2]
* Update Bedrock to 1.20.15.01

[2.23.0]
* Update Bedrock to 1.20.30.02

[2.24.0]
* Update Bedrock to 1.20.31.01

[2.25.0]
* Update Bedrock to 1.20.32.03

[2.26.0]
* Update base image to 4.2.0

[2.27.0]
* Update Bedrock to 1.20.40.01

[2.27.1]
* Update Bedrock to 1.20.41.02

[2.27.2]
* Update Bedrock to 1.20.50.03

[2.27.3]
* Update Bedrock to 1.20.51.01

[2.27.4]
* Update Bedrock to 1.20.61.01

[2.27.5]
* Update Bedrock to 1.20.62.02

[2.27.6]
* Update Bedrock to 1.20.62.03

[2.27.7]
* Update Bedrock to 1.20.71.01

[2.27.8]
* Update Bedrock to 1.20.72.01

[2.27.9]
* Update Bedrock to 1.20.73.01

[2.27.10]
* Update Bedrock to 1.20.80.05

[2.27.11]
* New repo structure

[2.27.12]
* Update Bedrock to 1.20.81.01

[2.28.0]
* Update Bedrock to 1.21.0.03

[2.28.1]
* Update Bedrock to 1.21.1.03

[2.28.2]
* Update Bedrock to 1.21.2.02

[2.29.0]
* Update Bedrock to 1.21.3.01

[2.29.1]
* Update Bedrock to 1.21.20.03

[2.30.0]
* Update Bedrock to 1.21.22.01

[2.31.0]
* Update Bedrock to 1.21.23.01

[2.32.0]
* Update Bedrock to 1.21.30.03

[2.33.0]
* Update Bedrock to 1.21.31.04

[2.33.1]
* Update Bedrock to 1.21.41.01

[2.33.2]
* Update Bedrock to 1.21.43.01

[2.33.3]
* Update Bedrock to 1.21.44.01

[2.33.4]
* Update bedrock to 1.21.50.10

[2.33.5]
* Update bedrock to 1.21.51.01

[2.33.6]
* Update bedrock to 1.21.51.02

[2.33.7]
* Update bedrock to 1.21.60.10

[2.34.0]
* Update bedrock to 1.21.61.01

[2.34.1]
* Update bedrock to 1.21.62.01

